<?php

return [
    'spotify'=>[
        'auth'=>[
            'base_url'=>'https://accounts.spotify.com/api/token',
            'basic_scheme' => 'Basic ',
            'bearer_scheme'=>'Bearer ',
            'grant_type'=>'client_credentials'
        ],
        'api'=>[
            'base_url'=>'https://api.spotify.com/v1/',
            'search_path'=>'search',
            'default_search_type'=> 'artist',
            'albums_path'=>'artists/@artist/albums',
            'limit_artists'=> 1,
            'limit_albums'=> 50,
            'include_groups' => 'album'
        ],
        'app'=>[
            'client_id'=>'634749ef93494caf8a39ebec7131adf4',
            'secret'=>'f88ec1959b3d4352b4b7b3c59124397e'
        ]
    ]
];
<?php

return [
    \Rainier\Logging\Providers\MonologServiceProvider::class,
    \Rainier\Controllers\Providers\ControllerProvider::class,
    \Rainier\Services\Providers\ExternalServiceProvider::class,
    \Rainier\ErrorHandlers\Providers\ErrorHandlerProvider::class
];
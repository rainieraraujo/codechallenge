<?php

return [
    'displayErrorDetails' => true,
    'external' => require __DIR__.'/external.php',
    'dependencies' => require __DIR__ . '/dependencies.php',
];
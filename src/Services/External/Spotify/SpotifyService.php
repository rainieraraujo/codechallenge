<?php

namespace Rainier\Services\External\Spotify;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Rainier\Exceptions\ArtistAlbumsNotFoundException;
use Rainier\Exceptions\ArtistNotFoundException;

class SpotifyService implements SpotifyServiceInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private $serviceFactory;
    private $spotifyToken;

    public function __construct($serviceFactory){
        $this->serviceFactory = $serviceFactory;
        unset($serviceFactory);
    }

    public function getAlbumsByArtist($artistName){
        $this->spotifyToken = $this->spotifyAuthenticate();
        $artistId = $this->spotifyGetArtist($artistName);
        $albums = $this->spotifyGetArtistAlbums($artistId);
        return $albums;
    }

    private function spotifyAuthenticate(){
        $authRequest = $this->serviceFactory->factoryMethod('auth');
        $response = $authRequest->sendRequest();
        return json_decode($response)->access_token;
    }

    private function spotifyGetArtist($artistName){
        $artistRequest = $this->serviceFactory->factoryMethod('artists');
        $params = [
            'artistName' => $artistName,
            'token' => $this->spotifyToken
        ];
        $response = $artistRequest->sendRequest($params);
        $artist = json_decode($response)->artists;
        if(!is_array($artist->items) OR empty($artist->items)){

            throw new ArtistNotFoundException();
        }
        return $artist->items[0]->id;
    }

    private function spotifyGetArtistAlbums($artistId){
        $albumsRequest = $this->serviceFactory->factoryMethod('albums');
        $params = [
            'artistId' => $artistId,
            'token' => $this->spotifyToken
        ];
        $response = $albumsRequest->sendRequest($params);
        $albums = json_decode($response);
        if(!is_array($albums->items) OR empty($albums->items)){
            throw new ArtistAlbumsNotFoundException();
        }
        return json_decode($response);
    }
}
<?php

namespace Rainier\Services\Providers;

use GuzzleHttp\ClientInterface;
use Rainier\Requests\SpotifyRequestFactory;
use Rainier\Services\External\Spotify\SpotifyService;
use GuzzleHttp\Client;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Rainier\Services\External\Spotify\SpotifyServiceInterface;

class ExternalServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['guzzle.client'] = function () use ($pimple):ClientInterface{
            return new Client();
        };

        $pimple['spotify.service'] = function () use ($pimple):SpotifyServiceInterface{
            $service = new SpotifyService(
                $pimple['spotify.request.factory']
            );
            $service->setLogger($pimple['logger']);
            return $service;
        };

        $pimple['spotify.request.factory'] = function () use ($pimple){
            return new SpotifyRequestFactory(
                $pimple['guzzle.client'],
                $pimple['settings']['external']['spotify'],
                $pimple['logger']
            );
        };
    }
}
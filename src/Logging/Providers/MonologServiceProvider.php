<?php

namespace Rainier\Logging\Providers;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Psr\Log\LoggerInterface;

class MonologServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['logger'] = function () use ($pimple):LoggerInterface{
            $logger =  new Logger(
                'CodeChallenge'
            );
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../../../logs/app.log', Logger::DEBUG));
            return $logger;
        };
    }
}

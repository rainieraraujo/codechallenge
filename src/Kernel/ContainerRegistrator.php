<?php
namespace Rainier\Kernel;

use Psr\Container\ContainerInterface;

class ContainerRegistrator
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        unset($container);
    }

    public function registerDependencies()
    {
        $dependencies = $this->container->get('settings')['dependencies'];

        if (is_array($dependencies) AND !empty($dependencies)) {
            foreach ($dependencies as $dependency) {
                /**
                 * @var $instance ServiceProviderInterface
                 */
                $instance = new $dependency();

                $this->container->register($instance);

                unset($instance);
            }
        }
    }
}
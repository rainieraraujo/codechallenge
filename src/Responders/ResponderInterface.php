<?php

namespace Rainier\Responders;

interface ResponderInterface{
    public function buildResponse($payload);
}
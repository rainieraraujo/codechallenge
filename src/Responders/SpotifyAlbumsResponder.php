<?php

namespace Rainier\Responders;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Rainier\Exceptions\IncompatibleResponseDataException;

class SpotifyAlbumsResponder implements ResponderInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;
    public function buildResponse($payload){
        $albumsData = [];
        if(is_array($payload->items) AND !empty($payload->items)){
            foreach ($payload->items as $album) {
                $albumsData[] = [
                    'name'=>$album->name,
                    'released'=>date_format(date_create($album->release_date),'d-m-Y'),
                    'tracks'=>$album->total_tracks,
                    'cover'=>[
                        'height'=> $album->images[0]->height,
                        'width'=> $album->images[0]->width,
                        'url'=> $album->images[0]->url
                    ]
                ];
            }
            $this->logger->info('Spotify artist albums data',[
                'origin'=> __CLASS__.__METHOD__,
                'response'=> $albumsData
            ]);
            return $albumsData;
        }
        else{
            $this->logger->error('Spotify response format error',[
                'origin'=> __CLASS__.__METHOD__,
                'error'=> $payload
            ]);
            throw new IncompatibleResponseDataException();
        }

    }
}
<?php

namespace Rainier\Middlewares;

use Rainier\Exceptions\InvalidArgumentsException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;

class RequestParamsMiddleware
{
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response, callable $next)
    {
        $query = $request->getQueryParams();

        if(!empty($query['q']) AND is_string($query['q']))
        {
            return $next($request, $response);
        }
        else {
            throw new InvalidArgumentsException();
        }
    }
}
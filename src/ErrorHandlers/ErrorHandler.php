<?php

namespace Rainier\ErrorHandlers;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Slim\Handlers\Error;

class ErrorHandler extends Error implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Exception $exception)
    {
        $status = $exception->getCode() ?? 500;
        $data = [
            "status" => "error",
            "message" => $exception->getMessage(),
        ];
        $this->logger->error('Error Handler detected error',$data);
        $body = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        return $response
            ->withStatus($status)
            ->withHeader("Content-type", "application/json")
            ->write($body);
    }
}
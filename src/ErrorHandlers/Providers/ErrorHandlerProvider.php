<?php

namespace Rainier\ErrorHandlers\Providers;

use Rainier\ErrorHandlers\ErrorHandler;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ErrorHandlerProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['errorHandler'] = function () use ($pimple) {
            $service = new ErrorHandler();
            $service->setLogger($pimple['logger']);
            return $service;
        };
    }
}
<?php

namespace Rainier\Requests;

class SpotifyGetArtistRequest extends RequestAbstract
{
    public function sendRequest($params = null)
    {
        try{
            $artist = $this->client->get($this->config['api']['base_url'].$this->config['api']['search_path'],[
                'headers'=>[
                    'Authorization'=> $this->config['auth']['bearer_scheme'].$params['token']
                ],
                'query' => [
                    'q' => $params['artistName'],
                    'type'=>$params['searchType'] ?? $this->config['api']['default_search_type'],
                    'limit'=> $this->config['api']['limit_artists']
                ]
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            if($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400'){
                    $error['response'] = $e->getResponse();
                }
            }
            $this->logError($error);
        }catch(Exception $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            $this->logError($error);
        }
        $this->logSuccessfulResponse($artist->getBody());
        return $artist->getBody();
    }
}
<?php

namespace Rainier\Requests;

use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

abstract class RequestAbstract implements SpotiftyRequestInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __construct(ClientInterface $client, $config, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->config = $config;
        $this->logger = $logger;
        unset($client,$config,$logger);
    }

    protected function logError($error){
        $this->logger->error('Request Error',[
            'origin'=> __CLASS__.__METHOD__,
            'error'=> $error
        ]);
    }

    protected function logSuccessfulResponse($response){
        $this->logger->info('Spotify API Respose',[
            'origin'=> __CLASS__.__METHOD__,
            'response'=> $response
        ]);
    }

}
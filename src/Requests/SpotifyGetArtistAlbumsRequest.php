<?php

namespace Rainier\Requests;

class SpotifyGetArtistAlbumsRequest extends RequestAbstract
{
    public function sendRequest($params = null)
    {
        try{
            $requestPath = str_replace('@artist',$params['artistId'],$this->config['api']['albums_path']);
            $albums = $this->client->get($this->config['api']['base_url'].$requestPath,[
                'headers'=>[
                    'Authorization'=> $this->config['auth']['bearer_scheme'].$params['token']
                ],
                'query'=>[
                    'limit'=>$this->config['api']['limit_albums'],
                    'include_groups'=>$this->config['api']['include_groups']
                ]
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            if($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400'){
                    $error['response'] = $e->getResponse();
                }
            }
            $this->logError($error);
        }catch(Exception $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            $this->logError($error);
        }
        $this->logSuccessfulResponse($albums->getBody());
        return $albums->getBody();
    }
}
<?php

namespace Rainier\Requests;

interface SpotiftyRequestInterface
{
    public function sendRequest($params = null);

}
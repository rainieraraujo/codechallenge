<?php

namespace Rainier\Requests;

class SpotifyAuthenticateRequest extends RequestAbstract
{
    public function sendRequest($params = null){
        $credentials = $this->prepareCredentials();
        try{
            $authenticate = $this->client->post($this->config['auth']['base_url'],[
                'headers'=>[
                    'Authorization'=> $this->config['auth']['basic_scheme'].$credentials
                ],
                'form_params'=>[
                    'grant_type'=> $this->config['auth']['grant_type']
                ]
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            if($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400'){
                    $error['response'] = $e->getResponse();
                }
            }
            $this->logError($error);
        }catch(Exception $e){
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            $this->logError($error);
        }
        $this->logSuccessfulResponse($authenticate->getBody());
        return $authenticate->getBody();
    }

    private function prepareCredentials(){
        return base64_encode($this->config['app']['client_id'].':'.$this->config['app']['secret']);
    }
}
<?php

namespace Rainier\Requests;

use Psr\Http\Client\ClientInterface;

class SpotifyRequestFactory
{

    private $config;
    private $client;
    private $logger;
    public function __construct(ClientInterface $client, $config, $logger)
    {
        $this->config = $config;
        $this->client = $client;
        $this->logger = $logger;
        unset($client,$config,$logger);
    }

    public function factoryMethod($type): SpotiftyRequestInterface {
        switch (strtolower($type)) {
            case 'auth':
                $service = new SpotifyAuthenticateRequest(
                    $this->client,
                    $this->config,
                    $this->logger
                );

                break;
            case 'artists':
                $service = new SpotifyGetArtistRequest(
                    $this->client,
                    $this->config,
                    $this->logger
                );
                break;
            case 'albums':
                $service = new SpotifyGetArtistAlbumsRequest(
                    $this->client,
                    $this->config,
                    $this->logger
                );
                break;
            default:
                throw new \RuntimeException("Spotifty request '$type' is not defined");
        }
        return $service;
    }
}
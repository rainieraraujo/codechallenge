<?php

namespace Rainier\Controllers\v1;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Rainier\Responders\ResponderInterface;
use Rainier\Services\External\Spotify\SpotifyServiceInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class SpotifyAlbumController implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private $service;

    public function __construct(SpotifyServiceInterface $service, ResponderInterface $responder)
    {
        $this->service = $service;
        $this->responder = $responder;
        unset($service);
    }

    public function getAlbumsByArtist(RequestInterface $request, ResponseInterface $response){
        $this->logger->info('Request Recieved',[
            'origin'=>__CLASS__.__METHOD__,
            'request'=>$request
        ]);
        $albums = $this->service->getAlbumsByArtist($request->getQueryParam('q',''));
        return $response->withJson($this->responder->buildResponse($albums));
    }
}
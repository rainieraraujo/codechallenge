<?php

namespace Rainier\Controllers\Providers;

use Rainier\Controllers\v1\SpotifyAlbumController;
use Rainier\Responders\ResponderInterface;
use Rainier\Responders\SpotifyAlbumsResponder;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ControllerProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['spotifyalbum.controller'] = function () use ($pimple) {
            $service = new SpotifyAlbumController(
                $pimple['spotify.service'],
                $pimple['spotifyalbum.responder']
            );
            $service->setLogger($pimple['logger']);
            return $service;
        };
        $pimple['spotifyalbum.responder'] = function () use ($pimple):ResponderInterface {
            $service = new SpotifyAlbumsResponder();
            $service->setLogger($pimple['logger']);
            return $service;
        };
    }
}
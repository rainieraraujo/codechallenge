<?php
namespace Rainier\Builders;

use Rainier\Kernel\ContainerRegistrator;

class AppBuilder
{
    private $app;
    public function __construct()
    {
        $app = new \Slim\App([
            'settings'=> require __DIR__ . '/../../config/app.php',
        ]);
        $registrator = new ContainerRegistrator($app->getContainer());
        $registrator->registerDependencies();
        try {
            require __DIR__ . '/../../routes/api/v1/api.php';
        } catch (Exception $e) {
            exit('Require failed! Error: '.$e);
        }
        $this->app = $app;
    }

    public function getApp(){
        return $this->app;
    }
}
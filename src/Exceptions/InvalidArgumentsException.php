<?php

namespace Rainier\Exceptions;
use Throwable;

class InvalidArgumentsException extends \RuntimeException
{
    const MESSAGE = 'Missing Arguments';
    const CODE = 400;
    public function __construct($message = "", $code = null, Throwable $previous = null)
    {
        $this->message = $message ? $message : self::MESSAGE;
        $this->code =  $code ??  self::CODE;
    }
}
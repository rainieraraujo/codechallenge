<?php

namespace Rainier\Exceptions;
use Throwable;

class IncompatibleResponseDataException extends \RuntimeException
{
    const MESSAGE = 'Spotify Response format not supported';
    const CODE = 500;
    public function __construct($message = "", $code = null, Throwable $previous = null)
    {
        $this->message = $message ? $message : self::MESSAGE;
        $this->code =  $code ??  self::CODE;
    }
}
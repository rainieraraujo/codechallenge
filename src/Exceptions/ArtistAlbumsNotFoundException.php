<?php

namespace Rainier\Exceptions;
use Throwable;

class ArtistAlbumsNotFoundException extends \RuntimeException
{
    const MESSAGE = 'Requested Artist Albums not found on spotify';
    const CODE = 404;
    public function __construct($message = "", $code = null, Throwable $previous = null)
    {
        $this->message = $message ? $message : self::MESSAGE;
        $this->code =  $code ??  self::CODE;
    }
}
<?php

use Rainier\Middlewares\RequestParamsMiddleware;

$app->get('/api/v1/albums','spotifyalbum.controller:getAlbumsByArtist')->add(new RequestParamsMiddleware());

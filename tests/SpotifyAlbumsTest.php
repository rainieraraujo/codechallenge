<?php
use Rainier\Builders\AppBuilder;
use Slim\Http\Environment;
use Slim\Http\Request;

class SpotifyAlbumsTest extends \PHPUnit\Framework\TestCase
{
    private $app;

    const URI = '/api/v1/albums?q=disturbed';
    const NONEXISTANT_ARTIST_ALBUMS_URI = '/api/v1/albums?q=aaaaaaaaaaaaa';
    const NONEXISTANT_ARTIST_URI = '/api/v1/albums?q=nonexistantartist';
    const MISSING_ARGUMENTS_URI = '/api/v1/albums';
    const EMPTY_ARGUMENTS_URI = '/api/v1/albums?q=';
    const NONEXISTANT_ARTIST_ERROR_MESSAGE = 'Requested Artist not found on spotify';
    const NONEXISTANT_ARTIST_ALBUMS_ERROR_MESSAGE = 'Requested Artist Albums not found on spotify';
    const MISSING_ARGUMENTS_ERROR_MESSAGE = 'Missing Arguments';


    public function setUp():void {
        $this->app = (new AppBuilder())->getApp();
    }

    public function testGetSpotifyArtistAlbums(){
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => self::URI,
        ]);
        $request = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $request;
        $response = $this->app->run(true);
        $albums = json_decode($response->getBody());
        $this->assertSame($response->getStatusCode(), 200);
        $this->assertIsArray($albums);
        $this->assertObjectHasAttribute('name',$albums[0]);
        $this->assertObjectHasAttribute('released',$albums[0]);
        $this->assertObjectHasAttribute('tracks',$albums[0]);
        $this->assertObjectHasAttribute('cover',$albums[0]);
        $this->assertIsString($albums[0]->name);
        $this->assertIsString($albums[0]->released);
        $this->assertIsInt($albums[0]->tracks);
        $this->assertIsObject($albums[0]->cover);
        $this->assertObjectHasAttribute('height',$albums[0]->cover);
        $this->assertObjectHasAttribute('width',$albums[0]->cover);
        $this->assertObjectHasAttribute('url',$albums[0]->cover);
        $this->assertIsInt($albums[0]->cover->height);
        $this->assertIsInt($albums[0]->cover->width);
        $this->assertIsString($albums[0]->cover->url);
    }

    public function testGetSpotifyNonexistantArtistAlbums(){
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => self::NONEXISTANT_ARTIST_URI,
        ]);
        $request = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $request;
        $response = $this->app->run(true);
        $albums = json_decode($response->getBody());
        $this->assertSame($response->getStatusCode(), 404);
        $this->assertIsObject($albums);
        $this->assertObjectHasAttribute('status',$albums);
        $this->assertObjectHasAttribute('message',$albums);
        $this->assertIsString($albums->status);
        $this->assertIsString($albums->message);
        $this->assertEquals('error',$albums->status);
        $this->assertEquals(self::NONEXISTANT_ARTIST_ERROR_MESSAGE,$albums->message);
    }

    public function testGetSpotifyArtistNonexistantAlbums(){
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => self::NONEXISTANT_ARTIST_ALBUMS_URI,
        ]);
        $request = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $request;
        $response = $this->app->run(true);
        $albums = json_decode($response->getBody());
        $this->assertSame($response->getStatusCode(), 404);
        $this->assertIsObject($albums);
        $this->assertObjectHasAttribute('status',$albums);
        $this->assertObjectHasAttribute('message',$albums);
        $this->assertIsString($albums->status);
        $this->assertIsString($albums->message);
        $this->assertEquals('error',$albums->status);
        $this->assertEquals(self::NONEXISTANT_ARTIST_ALBUMS_ERROR_MESSAGE,$albums->message);
    }

    public function testGetSpotifyArtistAlbumsMissingArgument(){
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => self::MISSING_ARGUMENTS_URI,
        ]);
        $request = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $request;
        $response = $this->app->run(true);
        $albums = json_decode($response->getBody());
        $this->assertSame($response->getStatusCode(), 400);
        $this->assertIsObject($albums);
        $this->assertObjectHasAttribute('status',$albums);
        $this->assertObjectHasAttribute('message',$albums);
        $this->assertIsString($albums->status);
        $this->assertIsString($albums->message);
        $this->assertEquals('error',$albums->status);
        $this->assertEquals(self::MISSING_ARGUMENTS_ERROR_MESSAGE,$albums->message);
    }

    public function testGetSpotifyArtistAlbumsEmptyArgument(){
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => self::EMPTY_ARGUMENTS_URI,
        ]);
        $request = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $request;
        $response = $this->app->run(true);
        $albums = json_decode($response->getBody());
        $this->assertSame($response->getStatusCode(), 400);
        $this->assertIsObject($albums);
        $this->assertObjectHasAttribute('status',$albums);
        $this->assertObjectHasAttribute('message',$albums);
        $this->assertIsString($albums->status);
        $this->assertIsString($albums->message);
        $this->assertEquals('error',$albums->status);
        $this->assertEquals(self::MISSING_ARGUMENTS_ERROR_MESSAGE,$albums->message);
    }
}
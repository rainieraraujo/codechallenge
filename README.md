# CodeChallenge

Un interesante code challenge que involucra el API de spotify en su modalidad Server to Server.

La solución implementa:
- Slim Framework 3
- Guzzle 7.0
- Monolog 2.1 (PSR-3 via PSR-Log)
- PHPUnit 9.4

EL scafolding del proyecto busca mantener lo más posible el SRP, considerando el alcance solicitado.

Mejoras posibles:

- Refactorizar el bootstrap: Por motivos de seguridad seria ideal manejar un directorio public que contenga el archivo index. Para aplicar esta solución de manera apropiada es necesario realizar configuraciones a nivel del web server.

- Añadir Annotations para proteger el tipado de las implementaciones concretas. 


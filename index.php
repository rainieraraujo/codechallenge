<?php
require __DIR__ . '/vendor/autoload.php';

$app = (new \Rainier\Builders\AppBuilder())->getApp();

$app->run();